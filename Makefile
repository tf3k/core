
# Run the CLI in interactive mode
cli:
	go run ./

# Run the tests
test:
	go test -cover -coverprofile=coverage.out ./... && go tool cover -func coverage.out

bubble:
	go run ./bubbletea/examples/views

# See here for more info:
# https://freshman.tech/linting-golang/
lint:
	golangci-lint run

# Visualize what lines aren't covered - useful for finding where you need more tests
cover-html:
	go test -cover -coverprofile=coverage.out ./... && go tool cover -html=coverage.out

val:
	make test && make lint
