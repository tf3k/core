# TF3-Core


## Description
This is an attempt at building out a library that implements the core logic for
a ToDo list that was inspired by the AutoFocus method.

You can read more about the AutoFlow method here: https://www.artofmanliness.com/character/behavior/autofocus-the-productivity-system-that-treats-your-to-do-list-like-a-river/

## Badges

For those who need stinking badges:

[![pipeline status](https://gitlab.com/tf3k/core/badges/master/pipeline.svg)](https://gitlab.com/tf3k/core/-/commits/master)

[![coverage report](https://gitlab.com/tf3k/core/badges/master/coverage.svg)](https://gitlab.com/tf3k/core/-/commits/master)

[![Latest Release](https://gitlab.com/tf3k/core/-/badges/release.svg)](https://gitlab.com/tf3k/core/-/releases)

## Installation
None yet.  I guess you can install the library with something like "go get
gitlab.com/tf3k/core", if you want.  But stay tuned...

## Usage
Not much to use, yet.  But stay tuned...

## Support
Create an Issue, add a "Bug" label if it's a bug, or "Feature" if it's an idea
for some new functionality.

## Roadmap
I'd like to:
* build out the core engine library first, 
* then build out a CLI that wraps the library, 
* then add a web service that exposes the library functionality,
* then update the CLI to be able to talk to the web service,
* then add a rich web app that talks to the web service,
* then maybe add a mobile app version?

## Contributing
The pipeline better pass, and you better not mess with what's in the pipeline
to make it pass.

All tests must pass.  Code coverage must not drop from one MR to the next,
unless there's a really good reason - and "I don't feel like writing tests" is
NOT a good reason.

Run "make test" to run the tests and see the coverage.

## Authors and acknowledgment
So far, just me.  But thanks to all my friends who have had long discussions
over the years with me about software stuff - you know who you are.

## License
3-Clause BSD License - see LICENSE for more info.


## Test and Deploy

I'm leaving this section in here because it might come in handy when it comes
time to deploy stuff.

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)
