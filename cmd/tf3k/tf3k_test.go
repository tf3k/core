package tf3k

import (
	"testing"

	tea "github.com/charmbracelet/bubbletea"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tf3k/core/cmd/tf3k/viewtypes"
)

func TestTf3k(t *testing.T) {
	t.Run("should make an initial model", func(t *testing.T) {
		got := InitialModel()
		assert.NotNil(t, got)
		assert.NotNil(t, got.notebook)
	})

	t.Run("should be able to call Init", func(t *testing.T) {
		m := InitialModel()
		got := m.Init()
		assert.Nil(t, got)
	})

	t.Run("should be able to call Update", func(t *testing.T) {
		m := InitialModel()
		m.viewType = viewtypes.ShowTaskList
		newM, cmd := m.Update(nil)
		assert.NotNil(t, newM)
		assert.Nil(t, cmd)

		m.viewType = viewtypes.ShowAddTask
		newM, cmd = m.Update(nil)
		assert.NotNil(t, newM)
		assert.Nil(t, cmd)

		m.viewType = viewtypes.InvalidView
		newM, cmd = m.Update(nil)
		assert.NotNil(t, newM)
		assert.Nil(t, cmd)
	})

	t.Run("<esc> in AddTaskView should go back to ListView", func(t *testing.T) {
		m := InitialModel()
		m.viewType = viewtypes.ShowAddTask
		newM, cmd := m.Update(tea.KeyMsg{Type: tea.KeyEscape})
		assert.NotNil(t, newM)
		assert.Nil(t, cmd)
		assert.IsType(t, newM, model{})
		realM, ok := newM.(model)
		assert.True(t, ok)
		assert.Equal(t, viewtypes.ShowTaskList, realM.viewType)
	})

	t.Run("<esc> in ShowListView should quit", func(t *testing.T) {
		m := InitialModel()
		m.viewType = viewtypes.ShowTaskList
		newM, cmd := m.Update(tea.KeyMsg{Type: tea.KeyEscape})
		assert.NotNil(t, newM)
		assert.NotNil(t, cmd)
		// There does not appear to be a good way to make sure the cmd is
		// Quit...
		//
		// This fails because == cannot compare func types:
		// 		assert.Equal(t, tea.Quit, cmd)
		// Also, cmd is of type tea.Cmd,
		// but tea.Quit is of type "func() tea.Msg"
		// ...
		// Also also, you can't compare the address of the function pointers,
		// even though they look the same in debug output...
	})

	t.Run("<a> in ShowListView should switch to AddTaskView", func(t *testing.T) {
		m := InitialModel()
		m.viewType = viewtypes.ShowTaskList
		newM, cmd := m.Update(
			tea.KeyMsg{
				Type:  tea.KeyRunes,
				Runes: []rune{'a'},
				Alt:   false,
			},
		)
		assert.NotNil(t, newM)
		assert.Nil(t, cmd)
		realM, ok := newM.(model)
		assert.True(t, ok)
		assert.Equal(t, viewtypes.ShowAddTask, realM.viewType)
	})

	t.Run("Should be able to get List View", func(t *testing.T) {
		m := InitialModel()
		m.viewType = viewtypes.ShowTaskList
		got := m.View()
		assert.NotEmpty(t, got)
	})

	t.Run("Should be able to get AddTask View", func(t *testing.T) {
		m := InitialModel()
		m.viewType = viewtypes.ShowAddTask
		got := m.View()
		assert.NotEmpty(t, got)
	})

	t.Run("View should handle invalid viewType", func(t *testing.T) {
		m := InitialModel()
		m.viewType = viewtypes.InvalidView
		got := m.View()
		assert.NotEmpty(t, got)
	})

	t.Run("Should be able to clear selections", func(t *testing.T) {

		// Make sure selections starts empty
		m := InitialModel()
		c := getChoices(m.notebook)
		assert.NotEmpty(t, c)
		assert.Empty(t, m.selected)

		// Select the first one
		m.selected[0] = struct{}{}
		assert.NotEmpty(t, m.selected)

		// Clear the selections and make sure it worked
		newM, cmd := m.Update(tea.KeyMsg{
			Type:  tea.KeyRunes,
			Runes: []rune{'c'},
			Alt:   false,
		})
		assert.Nil(t, cmd)
		realM, ok := newM.(model)
		assert.True(t, ok)
		assert.Empty(t, realM.selected)
	})

}
