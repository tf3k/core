package tf3k

import (
	"fmt"

	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/muesli/reflow/indent"
	"gitlab.com/tf3k/core/cmd/bubbles/addtask"
	"gitlab.com/tf3k/core/cmd/tf3k/viewtypes"
	"gitlab.com/tf3k/core/cmd/utils"
	"gitlab.com/tf3k/core/pkg/storage"
	"gitlab.com/tf3k/core/pkg/structures"
)

type model struct {
	viewType viewtypes.ViewType
	notebook *structures.Notebook
	addTask  tea.Model
	cursor   int              // which to-do list item our cursor is pointing at
	selected map[int]struct{} // which to-do items are selected
	err      error            // when something goes sideways
}

func InitialModel() model {
	nb := getDefaultNotebook()

	m := addtask.New(nb, textinput.New())
	addTask, ok := m.(addtask.AddTaskModel)
	if !ok {
		utils.Bail("could not cast AddTaskModel", -1)
	}

	return model{
		notebook: nb,
		addTask:  addTask,
		viewType: viewtypes.ShowTaskList,

		// A map which indicates which choices are selected. We're using
		// the  map like a mathematical set. The keys refer to the indexes
		// of the `choices` slice, above.
		selected: make(map[int]struct{}),
	}
}

// Gets the default notebook
func getDefaultNotebook() *structures.Notebook {
	// TODO: This ought to come from some persistent thing, perhaps use viper?
	// 		 ideally we'd look in some ~/.config/tf3k.conf for the default notebook
	// 		 value.
	result, err := structures.NewNotebook(
		"Default",
		&storage.MemoryStorage{},
	)
	utils.BailIfErrNotNil(err, "Could not create initial Notebook", -1)
	utils.BailIfErrNotNil(
		result.AddTask("make this work"),
		"could not add task",
		-1,
	)
	utils.BailIfErrNotNil(
		result.AddTask("make this work *better*"),
		"could not add task",
		-1,
	)
	return result
}

// Converts the list of tasks in the notebook into an array of strings
func getChoices(nb *structures.Notebook) []string {
	result := []string{}
	tasks, err := nb.GetEntries()
	utils.BailIfErrNotNil(err, "error getting entries", -1)
	for _, t := range tasks {
		result = append(result, t.GetText())
	}
	return result
}

func (m model) Init() tea.Cmd {
	// Just return `nil`, which means "no I/O right now, please."
	return nil
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {

	switch m.viewType {

	// If we're showing the AddTask dialog:
	case viewtypes.ShowAddTask:
		return handleAddTaskUpdate(m, msg)

	// If we're showing the list of tasks:
	case viewtypes.ShowTaskList:
		return handleShowTaskListUpdate(m, msg)
	}

	// Return the updated model to the Bubble Tea runtime for processing.
	// Note that we're not returning a command.
	return m, nil
}

func handleAddTaskUpdate(m model, msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		switch msg.Type {
		case tea.KeyEnter, tea.KeyEscape:
			m.viewType = viewtypes.ShowTaskList
		}
	}
	newAddTask, cmd := m.addTask.Update(msg)
	m.addTask = newAddTask
	return m, cmd
}

func handleShowTaskListUpdate(m model, msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {

	// Is it a key press?
	case tea.KeyMsg:
		return mainShowTaskListSwitch(m, msg.String())
	}
	return m, nil
}

func mainShowTaskListSwitch(m model, msg string) (tea.Model, tea.Cmd) {
	switch msg {

	// Add a new task
	case "a":
		m.viewType = viewtypes.ShowAddTask
		m.addTask = addtask.New(
			m.notebook,
			textinput.New(),
		)

	case "c":
		m.clearSelectedTasks()

	// Error!
	case "e":
		m.err = fmt.Errorf("let's just chew our way outta here")

	// These keys should exit the program.
	case "ctrl+c", "q", "esc":
		return m, tea.Quit

	// The "up" and "k" keys move the cursor up
	case "up", "k":
		if m.cursor > 0 {
			m.cursor--
		}

	// The "down" and "j" keys move the cursor down
	case "down", "j":
		choices := getChoices(m.notebook)
		if m.cursor < len(choices)-1 {
			m.cursor++
		}

	// The spacebar (a literal space) toggle
	// the selected state for the item that the cursor is pointing at.
	case " ":
		_, ok := m.selected[m.cursor]
		if ok {
			delete(m.selected, m.cursor)
		} else {
			m.selected[m.cursor] = struct{}{}
		}
	}
	return m, nil
}

func (m *model) clearSelectedTasks() {
	ents, err := m.notebook.GetEntries()
	utils.BailIfErrNotNil(err, "error getting notebook entries", -1)
	for i := range ents {
		_, ok := m.selected[i]
		if ok {
			delete(m.selected, i)
		}
	}
}

func (m model) View() string {
	// This is terrible:
	utils.BailIfErrNotNil(m.err, "error in View", -1)

	switch m.viewType {
	case viewtypes.ShowTaskList:
		return buildListView(m)
	case viewtypes.ShowAddTask:
		return buildAddTaskView(m)
	}
	return fmt.Sprintf("Invalid viewType: %d\n", m.viewType)
}

func buildAddTaskView(m model) string {
	s := "Add a task: " + m.addTask.View()
	return indent.String("\n"+s+"\n\n", 4)
}

func buildListView(m model) string {
	choices := getChoices(m.notebook)

	// The header
	s := "What to do?\n\n"

	// Iterate over our choices
	for i, choice := range choices {

		// Is the cursor pointing at this choice?
		cursor := " " // no cursor
		if m.cursor == i {
			cursor = ">" // cursor!
		}

		// Is this choice selected?
		checked := " " // not selected
		if _, ok := m.selected[i]; ok {
			checked = "x" // selected!
		}

		// Render the row
		s += fmt.Sprintf("%s [%s] %s\n", cursor, checked, choice)
	}

	// The footer
	s += "\nPress a to add a new task.\n"
	s += "Press <space> to toggle the current task's selection.\n"
	s += "Press c to clear selections.\n"
	s += "Press q (or escape, or ctrl-c) to quit.\n"

	// Send the UI for rendering
	return indent.String("\n"+s+"\n\n", 4)
}
