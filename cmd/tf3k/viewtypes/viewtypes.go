package viewtypes

type ViewType uint

const (
	InvalidView ViewType = iota
	ShowTaskList
	ShowAddTask
)
