package addtask

import (
	"fmt"
	"testing"

	"github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/stretchr/testify/assert"
	"gitlab.com/tf3k/core/pkg/structures"
)

func TestAddTask(t *testing.T) {
	t.Run("Update should add a new task", func(t *testing.T) {
		// Press "enter"...
		addTaskP := executeKeypress(
			t,
			getAndPopulateAddTask(t),
			tea.KeyMsg{Type: tea.KeyEnter},
		)

		ents, err := addTaskP.theNotebook.GetEntries()
		assert.Nil(t, err)
		assert.True(
			t,
			len(ents) == 1,
			fmt.Sprintf(
				"want there to be 1 entry, but there are %d",
				len(ents),
			),
		)
	})

	t.Run("Should clear input on esc", func(t *testing.T) {
		// Press "esc"...
		addTaskP := executeKeypress(
			t,
			getAndPopulateAddTask(t),
			tea.KeyMsg{Type: tea.KeyEscape},
		)

		// value should be empty
		val := addTaskP.Value()
		assert.Empty(t, val)
	})
}

func executeKeypress(t *testing.T, m tea.Model, msg tea.KeyMsg) *AddTaskModel {
	m, _ = m.Update(msg)
	addTask, ok := m.(AddTaskModel)
	if !ok {
		t.Errorf("could not cast Model to AddTaskModel")
		t.FailNow()
	}
	return &addTask
}

func keyPress(key rune) tea.Msg {
	return tea.KeyMsg{Type: tea.KeyRunes, Runes: []rune{key}, Alt: false}
}

func getAndPopulateAddTask(t *testing.T) *AddTaskModel {
	nb, err := structures.NewNotebook(
		"testing",
		&structures.MockStorageAPI{},
	)
	assert.Nil(t, err, "could not create test notebook")

	theInput := textinput.New()

	m := New(nb, theInput)

	taskStr := "hello world"
	for _, k := range taskStr {
		m, _ = m.Update(keyPress(k))
	}

	addTask := convertToAddTaskModel(t, m)
	val := addTask.Value()
	assert.Equal(t, taskStr, val, "should have entered the text")

	return addTask
}

func convertToAddTaskModel(t *testing.T, m tea.Model) *AddTaskModel {
	result, ok := m.(AddTaskModel)
	if !ok {
		t.Errorf("could not cast Model to AddTaskModel")
		t.FailNow()
	}
	return &result
}
