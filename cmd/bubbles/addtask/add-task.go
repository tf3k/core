package addtask

import (
	"fmt"

	ti "github.com/charmbracelet/bubbles/textinput"
	tea "github.com/charmbracelet/bubbletea"
	"gitlab.com/tf3k/core/cmd/utils"
	"gitlab.com/tf3k/core/pkg/structures"
)

type AddTaskModel struct {
	theInput    ti.Model
	theNotebook *structures.Notebook
	Error       error
}

func (m AddTaskModel) Init() tea.Cmd {
	return nil
}

func (m AddTaskModel) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	var cmd tea.Cmd

	switch msg := msg.(type) {

	// Is it a key press?
	case tea.KeyMsg:
		switch msg.Type {
		// Make a new Task
		case tea.KeyEnter:
			txtVal := m.theInput.Value()
			if txtVal != "" {
				// TODO: Probably need a better way of handling this?
				m.Error = m.theNotebook.AddTask(txtVal)
				utils.BailIfErrNotNil(
					m.Error,
					"error adding task",
					-1,
				)
			}
			m.theInput.Reset()
			m.theInput.Blur()

		// Clear the buffer
		case tea.KeyEsc:
			m.theInput.Reset()
		}
	}
	m.theInput, cmd = m.theInput.Update(msg)
	return m, cmd
}

func (m AddTaskModel) View() string {
	return m.theInput.View()
}

func (m AddTaskModel) Value() string {
	return m.theInput.Value()
}

func New(nb *structures.Notebook, theInput ti.Model) tea.Model {
	theInput.Focus()
	theInput.EchoMode = ti.EchoNormal
	theInput.Placeholder = "[task description]"
	theInput.Width = 80
	theInput.CharLimit = 80
	theInput.Validate = func(s string) error {
		if s == "" {
			return fmt.Errorf("cannot be empty")
		}
		return nil
	}
	return AddTaskModel{
		theNotebook: nb,
		theInput:    theInput,
	}
}
