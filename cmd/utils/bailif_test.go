package utils

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBailIfErrNotNil(t *testing.T) {
	t.Run("should bail given error", func(t *testing.T) {
		UseMockExitFunc(MockExit)
		want := DEFAULT_EXIT_ERROR_VAL
		BailIfErrNotNil(
			fmt.Errorf("fake error"),
			"I just want my truck back!",
			want,
		)
		assert.Equal(t, want, GetMockExitValue())
	})
}

func TestBailIfNil(t *testing.T) {
	t.Run("should bail given thing is nil", func(t *testing.T) {
		UseMockExitFunc(MockExit)
		BailIfNil(nil, "wat", DEFAULT_EXIT_ERROR_VAL)
		assert.Equal(t, DEFAULT_EXIT_ERROR_VAL, GetMockExitValue())
	})
}

func TestUseMockExit(t *testing.T) {
	t.Run("resets code when called", func(t *testing.T) {
		mockCode = 5
		UseMockExitFunc(MockExit)
		assert.Equal(t, DEFAULT_EXIT_VAL, GetMockExitValue())
	})
}
