package utils

import (
	"fmt"
	"os"
)

var privateExitFunc func(code int) = os.Exit

func Bail(msg string, code int) {
	fmt.Printf("bailing: %s\n", msg)
	privateExitFunc(code)
}

func BailIfErrNotNil(err error, msg string, code int) {
	if err != nil {
		fmt.Printf("error: %s: %v\n", msg, err)
		privateExitFunc(code)
	}
}

func BailIfNil(thing interface{}, msg string, code int) {
	if thing == nil {
		fmt.Printf("error: thing is nil: %s\n", msg)
		privateExitFunc(code)
	}
}

const DEFAULT_EXIT_VAL = 0
const DEFAULT_EXIT_ERROR_VAL = -1

var mockCode = DEFAULT_EXIT_VAL

func UseMockExitFunc(fn func(int)) {
	privateExitFunc = fn
	mockCode = DEFAULT_EXIT_VAL
}

func MockExit(c int) {
	mockCode = c
}

func GetMockExitValue() int {
	return mockCode
}
