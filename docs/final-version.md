# Final Version

This is a blatant copy for archival purposes of the Final Version of the
AutoFocus system, by Mark Forster.

I found it here: http://archive.constantcontact.com/fs004/1100358239599/archive/1109511856508.html

## Introduction
Here are the long-awaited instructions for the Final Version (FV) time
management system. I don't know if it's the best time management system ever
devised. What I do know is that it is the best time management system that I
have ever used myself. It's shown itself to be resilient, responsive and very
quick.

FV is based on my earlier time management systems, particularly the extensive
range of AutoFocus and SuperFocus systems developed over the last three years.
These were unique in that they were constantly developing with the assistance
of a large band of commenters on my web-site. Anyone who has tried one or more
of these systems will recognize the strong family resemblance that they have
with FV. The most striking resemblance is that they are all based on one long
list (either paper or electronic) which can be used to capture just about every
possible action that springs into one's mind. There is a minimum of special
markings or annotations.

Such a list depends on an effective algorithm to process it. There are three
main requirements which have to be kept in balance. These are urgency,
importance and psychological readiness. Traditional time management systems
have tended to concentrate on the first two of these. The neglect of
psychological readiness is probably the reason that most people don't find time
management systems particularly effective or congenial.

The most distinctive feature of FV is the way that its algorithm is primarily
based on psychological readiness - this then opens the way to keeping urgency
and importance in the best achievable balance.

## The FV  Algorithm
Anyone who has followed the discussions on my website will recognize that the
FV algorithm is loosely based on two powerful methods of making a decision,
"structured procrastination" and Colley's rule. I don't intend to go into
either of these now as an understanding of them is not relevant to the finished
algorithm, but anyone who wants to know more about them can google them.

The FV algorithm uses the question "What do I want to do before I do x?" to
preselect a chain of tasks from the list. What exactly is meant by "want" in
this context is deliberately left undefined. There may be a whole variety of
reasons why you might want to do one thing before another thing and all of them
are valid.

The chain always starts with the first unactioned task on the list. Mark this
task with a dot to show that it's now been preselected. Don't take any action
on the task at this stage.

This task then becomes the benchmark from which the next task is selected. For
example, if the first task on the list is "Write Report", the question becomes
"What do I want to do before I write the report?" You move through the list in
order until you come to a task which you want to do before writing the report.
This task is now selected by marking it with a dot and it becomes the benchmark
for the next task. If the first task you come to which you want to do before
writing the report is "Check Email", then that becomes the benchmark. The
question therefore changes to "What do I want to do before I check email?"

As you continue through the list you might come to Tidy Desk and decide you
want to do that before checking email. Select this in the same way by marking
it with a dot, and change the question to "What do I want to do before tidying
my desk?". The answer to this is probably "nothing", so you have now finished
your preselection.

The preselected tasks in the example are:

```
  Write report
  Check email
  Tidy desk
```

Do these in reverse order, i.e. Tidy desk, Check email, Write report. Note that
as in all my systems, you don't have to finish the task - only do some work on
it. Of course if you do finish the task that's great, but if you don't then all
you have to do is re-enter the task at the end of the list.

Once you have taken action on all the preselected tasks, preselect another
chain of tasks starting again from the first unactioned task on the list.

That's it! You're now ready to go. Everything else is further examples and
explanation.

## A Longer Example

In this example for ease of understanding no new tasks are added while working
on the list. This of course is unlikely in real life.

Your initial list of tasks:

```
  Email
  In-Tray
  Voicemail
  Project X Report
  Tidy Desk
  Call Dissatisfied Customer
  Make Dental Appointment
  File Invoices
  Discuss Project Y with Bob
  Back Up
```

Put a dot in front of the first task:

```
* Email
  In-Tray
  Voicemail
  Project X Report
  Tidy Desk
  Call Dissatisfied Customer
  Make Dental Appointment
  File Invoices
  Discuss Project Y with Bob
  Back Up
```

Now ask yourself " What do I want to do before I do Email?"

You work down the list and come to Voicemail. You decide you want to do
Voicemail before doing Email. Put a dot in front of it.

```
* Email
  In-Tray
* Voicemail
  Project X Report
  Tidy Desk
  Call Dissatisfied Customer
  Make Dental Appointment
  File Invoices
  Discuss Project Y with Bob
  Back Up
```

Now ask yourself " What do I want to do before I do Voicemail?" You decide you
want to tidy your desk.

```
* Email
  In-Tray
* Voicemail
  Project X Report
* Tidy Desk
  Call Dissatisfied Customer
  Make Dental Appointment
  File Invoices
  Discuss Project Y with Bob
  Back Up
```

There are no tasks you want to do before tidying your desk, so now take action
on the dotted tasks in reverse order:

```
* Tidy Desk
* Voicemail
* Email
```

Your list will now look like this (I've removed the tasks that have been
actioned but if you are using paper they will still be on the page but crossed
out):

```
  In-Tray
  Project X Report
  Call Dissatisfied Customer
  Make Dental Appointment
  File Invoices
  Discuss Project Y with Bob
  Back Up
```

Now start again with the first unactioned task on the list, In-Tray, and repeat
the same procedure. The only task you want to do before In-Tray is Back Up. As
this is the last task on the list there are only two dotted tasks:

```
* In-Tray
  Project X Report
  Call Dissatisfied Customer
  Make Dental Appointment
  File Invoices
  Discuss Project Y with Bob
* Back Up
```

Do the two dotted tasks in reverse order:

```
* Back Up
* In-Tray
```

So the list now looks like this:

```
  Project X Report
  Call Dissatisfied Customer
  Make Dental Appointment
  File Invoices
  Discuss Project Y with Bob
```

So far the tasks have been relatively trivial, but the Project X Report is
something that you have been putting off doing for a long time. So repeat the
procedure:

```
* Project X Report
  Call Dissatisfied Customer
* Make Dental Appointment
* File Invoices
  Discuss Project Y with Bob
```

You now file your invoices, make a dental appointment and make a start on the
Project X Report.

In your final pass through the list you Discuss Project Y with Bob and Call
Dissatisfied Customer.

So the tasks on the original list have been done in the following order. The
tasks in italics are the ones at the beginning of each scanning process.

```
  Tidy Desk
  Voicemail
  /Email/
  Back Up
  /In-Tray/
  File Invoices,
  Make Dental Appointment
  /Project X Report/
  Discuss Project Y with Bob
  /Call Dissatisfied Customer/
```


Notice what has happened here. The root tasks (the ones in italics) have been
done in strict list order, regardless of importance, urgency or any other
factor. Some of them are relatively easy (e.g. Email) and some are relatively
difficult (e.g. Project X Report) or you are reluctant to do them (e.g. Call
Dissatisfied Customer).


Each of the root tasks is preceded by a short ladder of tasks which are in the
order in which you want to do them. The number and difficulty of the tasks in
the ladder tend to reflect the difficulty of the root tasks.

## Additional Tips
The best way to sink any time management system is to overload it right at the
beginning. FV is pretty resilient, but at this stage you aren't. So build up
the list gradually. My advice is to start off with the tasks and projects that
are of immediate concern to you right now, and then add more as they come up in
the natural course of things.

Tasks can be added at any level, e.g. Project X, Plan Restructuring, Call Pete,
Tidy Desk.

If the first task on the list can't be done now for some valid reason (e.g.
wrong time of day, precondition not met, bad weather), then cross it out and
re-enter it at the end of the list. Use the next task as your starting
benchmark.

If at any stage you find that a task on the list is no longer relevant, then
delete it.

If you find that your preselected list is no longer relevant (e.g. if you have
had a long break away from the list), then scrap the preselection and reselect
from the beginning. A shorter way to do this is to reselect only from the last
preselected task which you haven't done yet.

If one or more very urgent things come up, write them at the end of the list
and mark them with a dot so that they are done next. If something already on
the list becomes very urgent, then move it to the end of the list and mark it
with a dot in the same way.

Remember that the aim of any time management system is to help you to get your
work done, not get in the way of doing your work. So don't be afraid to adjust
priorities if you need to. However try to keep this to a minimum - stick to the
rules whenever possible as they will ensure you deal with your work in a
systematic way.

## Why It Works
At the beginning of this newsletter I said there were three factors which every
time management system needs to address: urgency, importance and psychological
readiness. Let's see how FV deals with each of these.

Urgency. Because of the nature of the preselection process, urgent tasks tend
to get selected - generally speaking the human brain wants to do things that it
knows are urgent. If things come up that are particularly urgent they can be
added to the preselected list at any time.

Importance. Generally speaking the human brain is a bit less keen on doing
important stuff than it is on doing urgent stuff. This is particularly the case
when the important stuff is difficult. However the FV preselection process
ensures that tasks towards the beginning of the list are given as much
attention as tasks towards the end of the list.

Psychological Readiness. This is where FV really enters new dimensions. By
using a pre-selection process, the brain is softened up towards the selected
tasks. But this isn't all. The selection process is based on what you want to
do. This colours the whole preselected list so that even the first task, which
you may not have wanted to do at all, gets affected. In addition, doing the
list in reverse order, with the least wanted task last, uses structured
procrastination to get the tasks done.

## Yet to Come
This issue of the newsletter describes only the mechanics of the system. In
future issues I want to deal with such topics as:
* How to run projects
* Using the Little and Often principle
* FV in action
* How to use FV to best effect
etc, etc.

And most important of all a program of teleconferences and seminars so you can
really get the best out of what FV has to offer.
