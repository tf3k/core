package structures

import (
	"fmt"

	"gitlab.com/tf3k/core/pkg/structures/entries"
)

type Notebook struct {
	Name    string
	entries []entries.EntryInterface
	storage StorageAPI
}

func (n *Notebook) addEntry(e entries.EntryInterface) error {
	return n.storage.AddEntry(e)
}

func (n *Notebook) AddQuote(text string, author string, source string) error {
	return n.addEntry(
		entries.NewQuote(text, author, source),
	)
}

func (n *Notebook) AddTask(text string) error {
	return n.addEntry(
		entries.NewTask(text),
	)
}

func (n *Notebook) AddNote(text string) error {
	return n.addEntry(
		entries.NewEntry(text),
	)
}

func (n *Notebook) GetEntries() ([]entries.EntryInterface, error) {
	return n.storage.GetEntries()
}

func NewNotebook(name string, s StorageAPI) (*Notebook, error) {
	if s == nil {
		return nil, fmt.Errorf("nil Storage implementation")
	}
	return &Notebook{
		Name:    name,
		entries: []entries.EntryInterface{},
		storage: s,
	}, nil
}
