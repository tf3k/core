package structures

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewNotebook(t *testing.T) {
	t.Run("creates new notebook", func(t *testing.T) {
		wantName := "testing"
		n, err := NewNotebook(wantName, &MockStorageAPI{})
		assert.Nil(t, err)
		assert.Equal(t, wantName, n.Name)
	})
	t.Run("complains about nil Storage reference", func(t *testing.T) {
		n, err := NewNotebook("nope", nil)
		assert.Nil(t, n)
		assert.NotNil(t, err)
	})
}

func TestNotebook(t *testing.T) {
	nb, err := NewNotebook("testing", &MockStorageAPI{})
	assert.Nil(t, err)
	t.Run("adds a note", func(t *testing.T) {
		want := "testing"
		err := nb.AddNote(want)
		assert.Nil(t, err)

		afterLen := getEntriesLength(t, nb)
		assert.True(t, afterLen > 0)
	})
	t.Run("adds a Quote", func(t *testing.T) {
		beforeLength := getEntriesLength(t, nb)

		err = nb.AddQuote(
			"You would not enjoy Nietzsche, sir. He is fundamentally unsound.",
			"P.G. Wodehouse",
			"Carry on, Jeeves",
		)
		assert.Nil(t, err)

		afterLength := getEntriesLength(t, nb)
		assert.Greater(t, afterLength, beforeLength)
	})
	t.Run("adds a Task", func(t *testing.T) {
		beforeLength := getEntriesLength(t, nb)

		err = nb.AddTask(
			"Look up more Wodehouse quotes",
		)
		assert.Nil(t, err)

		afterLength := getEntriesLength(t, nb)
		assert.Greater(t, afterLength, beforeLength)
	})
}

func getEntriesLength(t *testing.T, nb *Notebook) int {
	entries, err := nb.GetEntries()
	assert.Nil(t, err)
	return len(entries)
}
