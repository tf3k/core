package structures

import "gitlab.com/tf3k/core/pkg/structures/entries"

type StorageAPI interface {
	AddEntry(entries.EntryInterface) error
	GetEntries() ([]entries.EntryInterface, error)
}

type MockStorageAPI struct {
	Entries []entries.EntryInterface
}

func (m *MockStorageAPI) AddEntry(ent entries.EntryInterface) error {
	m.Entries = append(m.Entries, ent)
	return nil
}

func (m *MockStorageAPI) GetEntries() ([]entries.EntryInterface, error) {
	return m.Entries, nil
}
