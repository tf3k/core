package entries

type Quote struct {
	Entry
	author string
	source string
}

func (q *Quote) GetAuthor() string {
	return q.author
}

func (q *Quote) GetSource() string {
	return q.source
}

func NewQuote(text string, author string, source string) Quote {
	return Quote{
		Entry:  NewEntry(text),
		author: author,
		source: source,
	}
}
