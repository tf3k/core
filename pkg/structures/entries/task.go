package entries

type Task struct {
	Entry
	status TaskStatus
}

func (t *Task) GetStatus() TaskStatus {
	return t.status
}

func NewTask(text string) Task {
	return Task{
		Entry:  NewEntry(text),
		status: Active,
	}
}
