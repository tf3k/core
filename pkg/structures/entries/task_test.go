package entries

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewTask(t *testing.T) {
	t.Run("makes a new task", func(t *testing.T) {
		task := NewTask("dummy description")
		assert.NotNil(t, task)
		assert.Equal(t, Active, task.GetStatus())
	})

}
