package entries

type TaskStatus int

const (
	Active TaskStatus = iota
	Complete
	Scrapped
	Migrated
	Invalid
)

func (s TaskStatus) String() string {
	switch s {
	case Active:
		return "Active"
	case Complete:
		return "Complete"
	case Scrapped:
		return "Scrapped"
	case Migrated:
		return "Migrated"
	}
	return "Invalid"
}
