package entries

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewQuote(t *testing.T) {
	t.Run("makes a Quote", func(t *testing.T) {
		wantAuthor := "P. G. Wodehouse"
		wantSource := "Very Good, Jeeves!"
		wantText := "Red hair, sir, in my opinion, is dangerous."
		quote := NewQuote(wantText, wantAuthor, wantSource)
		assert.Equal(t, wantAuthor, quote.GetAuthor())
		assert.Equal(t, wantSource, quote.GetSource())
		assert.Equal(t, wantText, quote.GetText())
	})
}
