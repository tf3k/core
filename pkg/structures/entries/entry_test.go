package entries

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestNewEntry(t *testing.T) {
	t.Run("makes a new Entry", func(t *testing.T) {
		want := "dummy description"
		before := time.Now().UTC()
		entry := NewEntry(want)
		assert.NotNil(t, entry)
		assert.Equal(t, want, entry.GetText())
		assert.True(t, entry.when.Equal(before) || entry.when.After(before))

		assert.Equal(t, want, entry.GetText())
		assert.Equal(t, entry.when, entry.GetWhen())
	})
}
