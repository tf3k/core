package entries

import "time"

type EntryInterface interface {
	GetText() string
	GetWhen() time.Time
}

type Entry struct {
	text string
	when time.Time
}

func (e Entry) GetText() string {
	return e.text
}

func (e Entry) GetWhen() time.Time {
	return e.when
}

func NewEntry(text string) Entry {
	return Entry{
		text: text,
		when: time.Now().UTC(),
	}
}
