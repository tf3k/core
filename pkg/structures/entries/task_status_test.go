package entries

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestStatus(t *testing.T) {
	t.Run("compare check", func(t *testing.T) {
		s := Active
		assert.NotEqual(t, s, Complete)
		assert.Equal(t, s, Active)
	})
	t.Run("strings are unique", func(t *testing.T) {
		statuses := []TaskStatus{
			Active, Complete, Scrapped, Migrated, Invalid,
		}
		strings := make(map[string]bool)

		for _, status := range statuses {
			assert.NotContains(t, strings, status.String())
			strings[status.String()] = true
		}
		assert.Equal(t, len(statuses), len(strings))
	})
}
