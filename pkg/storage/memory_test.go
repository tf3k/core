package storage

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/tf3k/core/pkg/structures"
	"gitlab.com/tf3k/core/pkg/structures/entries"
)

func TestMemoryStorage(t *testing.T) {
	m := new(MemoryStorage)
	theTask := entries.NewTask("testing")

	t.Run("must implement the interface", func(t *testing.T) {
		assert.Implements(t, (*structures.StorageAPI)(nil), m)
	})

	t.Run("adds a task", func(t *testing.T) {
		assert.Nil(t, m.AddEntry(theTask))
	})

	t.Run("gets task list", func(t *testing.T) {
		assert.Nil(t, m.AddEntry(theTask))
		got, err := m.GetEntries()
		assert.Nil(t, err)
		assert.NotEmpty(t, got)
	})
}
