package storage

import (
	"gitlab.com/tf3k/core/pkg/structures/entries"
)

type MemoryStorage struct {
	Entries []entries.EntryInterface
}

func (m *MemoryStorage) AddEntry(t entries.EntryInterface) error {
	m.Entries = append(m.Entries, t)
	return nil
}

func (m *MemoryStorage) GetEntries() ([]entries.EntryInterface, error) {
	return m.Entries, nil
}
